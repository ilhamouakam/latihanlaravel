<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class castController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        
            $request->validate([
                    'Namamu' => 'required',
                    'Umurmu' => 'required',
                    'Biomu' => 'required',
            ]);

            DB::table('cast')->insert([
                'Nama' => $request['Namamu'],
                'Umur' => $request['Umurmu'],
                'Bio' => $request['Biomu'],
            ]);

            return redirect('/cast');
    }

    public function index(){
        $VarCast = DB::table('cast')->get();
        return view('cast.index', compact('VarCast'));
    }

    public function show($id){
        $VarId = DB::table('cast')->where('id',$id)->first();
        return view('cast.show',compact('VarId'));
    }

    public function edit($id){
        $VarId = DB::table('cast')->where('id',$id)->first();
        return view('cast.edit',compact('VarId'));
    }

    public function update($id, Request $request){
        $request->validate([
            'Namamu' => 'required',
            'Umurmu' => 'required',
            'Biomu' => 'required',
    ]);
        $query =  DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
        ]);

        return redirect('/cast');

    }

    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
