@extends('layout.master')

@section('judul')
Buat Account Baru!
Sign Up Form
@endsection

@section('isicontent')

    
        <form action="/kirim" method="POST">
            @csrf
            <label>First Name:</label><br>
            <input type="text" name="FNmu"><br><br>

            <label>Last Name:</label><br>
            <input type="text" name="LNmu"><br><br>

            <label>Gender:</label><br>
            <input type="radio" name="JKmu" value="Lak">Male<br>
            <input type="radio" name="JKmu" value="Per">Female<br>
            <input type="radio" name="JKmu" value="Otr">Other<br><br>


            <label>Nationality:</label><br>
            <select name="Negeramu">
                <option value="ina">Indonesia</option>
                <option value="den">DenMark</option>
                <option value="bel">Belanda</option>
            </select><br><br>


            <label>Language Spoken:</label><br>
            <input type="checkbox" name="Bahasamu" value="Indo">Bahasa Indonesia<br>
            <input type="checkbox" name="Bahasamu" value="Ingg">English<br>
            <input type="checkbox" name="Bahasamu" value="Lain">Other<br><br>

            <label>Bio:</label><br>
            <textarea name="Biomu" cols="30"rows="10"></textarea><br>

        <button type="submit" value="kirim">Sign Up</button></a>
        </form>

@endsection