@extends('layout.master')

@section('judul')
Detail dari CAST {{$VarId->nama}}
@endsection

@section('isicontent')

<h3>Nama: {{$VarId->nama}}</h3>
<p>Umur: {{$VarId->umur}} <br>
Bio: {{$VarId->bio}}
</p>

<a href="/cast" class="btn btn-success mb-2" >Kembali ke Daftar CAST</a>

@endsection