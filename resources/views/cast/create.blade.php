@extends('layout.master')

@section('judul')
Formulir Isian CAST
@endsection

@section('isicontent')

    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama: </label>
            <input type="text" name="Namamu"><br>
    
                @error('Namamu')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
    
            <label>Umur: </label>
            <input type="number" name="Umurmu"><br>
    
                @error('Umurmu')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            
            <label>Bio:</label><br>
            <textarea name="Biomu" cols="30"rows="5"></textarea><br>
    
                @error('Biomu')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
    
        </div>

       
       
        <button type="submit" class="btn btn-primary">Submit</button>
        
    </form>

@endsection
