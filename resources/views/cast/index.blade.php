@extends('layout.master')

@section('judul')
Daftar List dari CAST
@endsection

@section('isicontent')

<a href="/cast/create" class="btn btn-success mb-2" >Tambah Data CAST</a>

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Aksi</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($VarCast as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->umur}}</td>
                    <td>{{$item->bio}}</td>

                    <td><a href="/cast/{{$item->id}}" class="btn btn-info btn-sm" >Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm" >Edit</a>
                        <form action="/cast/{{$item->id}}" method="POST">
                            @method('delete')
                            @csrf

                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">

                        </form>
                        
                    <td>
                </tr>
                    
                @empty
                <tr>
                    <td> Tidak ada DATA</td>
                </tr>
                    
                @endforelse
            </tbody>
        </table>
        
       

@endsection

