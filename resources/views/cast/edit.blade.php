@extends('layout.master')

@section('judul')
Formulir EDIT CAST {{$VarId->nama}}
@endsection

@section('isicontent')

    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Nama: </label>
            <input type="text" name="Namamu" value="{{$VarId->nama}}" class="fore-control"><br>
    
                @error('Namamu')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
    
            <label>Umur: </label>
            <input type="number" name="Umurmu" value="{{$VarId->umur}}"><br>
    
                @error('Umurmu')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            
            <label>Bio:</label><br>
            <textarea name="Biomu" cols="30"rows="5" value="{{$VarId->bio}}"></textarea><br>
    
                @error('Biomu')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
    
        </div>

       
       
        <button type="submit" class="btn btn-primary">Submit</button>
        
    </form>

@endsection
