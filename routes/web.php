<?php

/*
|--------------------------------------------------------------------------
| Web Routes Cipto Ilham Rudi
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Tugas
Route::get('/', 'IndexController@indexku');

Route::get('/form', 'AuthController@authku');

Route::POST('/kirim', 'AuthController@kirim');

Route::get('/data-tables', function () {
    return view('partial.data-tables');
});

Route::get('/table', function () {
    return view('partial.table');
});


//hari #15 CRUD Cast
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast', 'CastController@Index');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::get('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');

/* master Utama sudah di testing
Route::get('/master', function(){
    return view('layout.master');
});

*/


/*  LATIHAN System Route melalui VIEW
Route::get('/', function () {
    return view('welcome');
});


//ini contoh tanpa membuat folder dalam views
route::get('/helo', function() {
    return "Heloo Ilham";
});

//ini contoh dengan membuat folder dalam views
Route::get('/helo', function () {
    return view('halaman.index');
});

//ini contoh dengan sebuah formulir di dalam views
route::get('/form', function() {
    return view("halaman.form");
});

*/
